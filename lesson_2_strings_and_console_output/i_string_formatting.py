"""
When you want to print a variable with a string, there is a better method than concatenating strings together.
"""

string_1 = "Camelot"
string_2 = "place"

print "Let's not go to %s. 'Tis a silly %s." % (string_1, string_2)
