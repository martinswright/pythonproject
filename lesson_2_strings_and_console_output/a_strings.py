"""
    Another useful data type is the string. A string can contain letters, numbers, and symbols.
"""
brian="hello life"
aNumber = "42"
aSymbolsString = "@@&&$$"

print brian
print aNumber
print aSymbolsString
