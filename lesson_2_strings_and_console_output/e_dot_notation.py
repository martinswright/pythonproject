"""

 Methods that use dot notation only work with strings.
 
"""
ministry = "The Ministry of Silly Walks"
number = 2000
# the dot notation only works with strings

print len(ministry)
print ministry.upper()

# but len() and str() work with other data types


print str(number)
