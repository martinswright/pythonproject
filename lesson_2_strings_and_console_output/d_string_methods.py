"""
String methods let you perform specific tasks for strings.
 
"""
from string import lower
parrot="Norwegian Bluennnnnnnnnnnnnnnnn"
# print the length of the string
print len(parrot)
# get the string in lower case chars
print parrot.lower()
# get the string in upper case
print parrot.upper()
# str converts a variable to a String
pi=3.14
print str(pi)