"""
The break is a one-line statement that means "exit the current loop." 
An alternate way to make our counting loop exit and stop executing is with the break statement.
  
"""
count = 0

while True:
  print count
  count += 1
  if count >= 10:
    break

