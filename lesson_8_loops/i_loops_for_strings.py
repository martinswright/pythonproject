"""
Using a for loop, you can print out each individual character in a string.
"""

thing = "spam!"

for c in thing:
  print c

