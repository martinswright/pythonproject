"""
A while loop example
"""
num = 1
# go from 1 to 10
while num < 11:  
  # Print num squared
  print num**2
  # Increment num 
  num = num + 1
