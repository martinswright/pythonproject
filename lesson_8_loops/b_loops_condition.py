"""
The condition is the expression that decides whether the loop is going to continue being executed or not.
There are 5 steps to this program:

1 The loop_condition variable is set to True

2 The while loop checks to see if loop_condition is True. It is, so the loop is entered.

3 The print statement is executed.

4 The variable loop_condition is set to False.

5 The while loop again checks to see if loop_condition is True. It is not, so the loop is not executed a second time.
"""
loop_condition = True

while loop_condition:
  print "I am a loop"
  loop_condition = False