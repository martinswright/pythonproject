"""
The while loop is similar to an if statement: it executes the code inside of it if some condition is true. 
The difference is that the while loop will continue to execute as long as the condition is true.
"""
count = 0

if count < 5:
  print "Hello, I am an if statement and count is", count

while count < 5:
  print "Hello, I am a while and count is", count
  count += 1