"""

A common application of a while loop is to check user input to see if it is valid. For example, 
if you ask the user to enter y or n and they instead enter 7, then you should re-prompt them for input.
"""
choice = raw_input('Enjoying the course? (y/n)')

while choice != 'y' and choice != 'n':
  choice = raw_input("Sorry, I didn't catch that. Enter again: ")

print "thanks for your feedback."