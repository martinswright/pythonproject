"""
An infinite loop is a loop that never exits. This can happen for a few reasons:

The loop condition cannot possibly be false (e.g. while 1 != 2)

The logic of the loop prevents the loop condition from becoming false.
An example would be:
count = 10
while count > 0:
  count += 1 # Instead of count -= 1
  
"""
