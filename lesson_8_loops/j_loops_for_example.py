phrase = "A bird in the hand..."

""" 
replace A or a with X 
"""
      
for char in phrase:
  if char == "A" or char == "a":
    print "X",
  else:
    print char,

print