# The # sign is for comments. A comment is a line of text that Python won't try to run as code. It's just for humans to read.
""" multiline comments start and end with 3 double quotes
and allow you to write a load of text in a comment block
"""

