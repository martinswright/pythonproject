"""
Great! Now let's do some math. 
You can add, subtract, multiply, divide numbers like this
"""
addition = 72 + 23
subtraction = 108 - 204
multiplication = 108 * 0.5
division = 108 / 9

# Set count_to equal to the sum of two big numbers
count_to = 300 + 400

print count_to

"""
All that math can be done on a calculator, so why use Python? 
Because you can combine math with other data types (e.g. booleans) 
and commands to create useful programs. 
Calculators just stick to numbers.
"""
# Set eggs equal to 100 using exponentiation on line 3!

eggs = 10**2

print eggs

"""
Our final operator is modulo. 
Modulo returns the remainder from a division. So, if you type 3 % 2, it will return 1, 
because 2 goes into 3 evenly once, with 1 left over.
"""

#Set spam equal to 1 using modulo on line 3!

spam = 3 % 2

print spam
