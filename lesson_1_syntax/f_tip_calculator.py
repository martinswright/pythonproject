""" 
    Using things learnt so far we can right a tip calculator!
"""
# total cost of meal
meal = 44.50
# the tip as a percentage
tip = 15.0 / 100
# calculate the total cost
total = meal + meal * tip
# divide by the number of people
people = 4
perperson = total / people
# print the amount each person has to pay
print "cost of meal : " + str(meal)
print "percentage tip : " + str(tip)
print "total cost of meal : " + str(total)
print "cost per person : " + str(perperson)