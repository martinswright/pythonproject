""" Creating web apps, games, and search engines all involve storing and working with different types of data. 
They do so using variables.
"""
# A variable stores a piece of data, and gives it a specific name.
# variable can hold different data types
# a variable has to be assigned a value before it can be used.

# Booleans : hold True or False 
var1 = True
var2 = False
print var1
print var2

# integers - whole numbers
my_variable = 10
print my_variable

# variables can be re-assigned
myvar =  10
print myvar
myvar = 20
print myvar
