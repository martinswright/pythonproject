"""
Appending to the end of a list 
"""
letters = ['a', 'b', 'c']
print " length of list : " + str(len(letters))
print letters
letters.append('d')
print " length of list  after append : " + str(len(letters))
print letters