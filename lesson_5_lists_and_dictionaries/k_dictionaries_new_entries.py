"""
Like Lists, Dictionaries are mutable. This means they can be changed after they are created. 
One advantage of this is that we can add new key/value pairs to the dictionary after it is created
"""

menu = {} # Empty dictionary
menu['Chicken Alfredo'] = 14.50 # Adding new key-value pair
print menu['Chicken Alfredo'] # access the value by key

# Add some dish-price pairs to menu!
menu['Chicken'] = 1.00
menu['Alfredo'] = 10.0
menu['pasta'] = 12.00



print "There are " + str(len(menu)) + " items on the menu."
print menu