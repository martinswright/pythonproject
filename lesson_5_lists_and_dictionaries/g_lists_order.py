"""
finding a particular thing in a list and inserting something into a list
""" 
animals = ["aardvark", "badger", "duck", "emu", "fennec fox"]

print "animals before insert :" + str(animals)
# finding something in a list
duck_index = animals.index("duck") # Use index() to find "duck"
# insert cobra in front of duck and all the entries move down.
animals.insert(duck_index, "cobra")
print "animals after insert :" + str(animals)

