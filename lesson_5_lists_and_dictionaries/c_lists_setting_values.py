"""
A list index behaves like any other variable name! It can be used to access as well as assign values.
"""
# assignment

zoo_animals = ["pangolin", "cassowary", "sloth", "tiger"]

print "list contains : " + str(zoo_animals) 
# accessing an individual element
zoo_animals[2] = "hyena"

print "list now contains : "  +str( zoo_animals)