
"""
You can access an individual item on the list by its index. 
An index is like an address that identifies the item's place in the list. 
The index appears directly after the list name, in between brackets, like this: list_name[index].
The index begins at zero and NOT 1
"""
numbers = [5, 6, 7, 8]

print "Adding the numbers at indices 0 and 2..."
print numbers[0] + numbers[2]
print "Adding the numbers at indices 1 and 3..."
# Your code here!
print numbers[1] + numbers[3]