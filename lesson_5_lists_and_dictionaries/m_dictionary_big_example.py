inventory = {
  'gold' : 500,
  'pouch' : ['flint', 'twine', 'gemstone'], # Assigned a new list to 'pouch' key
  'backpack' : ['xylophone','dagger', 'bedroll','bread loaf']
}

# Adding a key 'burlap bag' and assigning a list to it
inventory['burlap bag'] = ['apple', 'small ruby', 'three-toed sloth']

# Sorting the list found under the key 'pouch'
inventory['pouch'].sort() 

# put stuff in pocket
inventory['pocket'] = ['seashell', 'strange berry', 'lint']
# sort stuff in backpack
inventory['backpack'].sort()
# remove dagger from backpack
inventory['backpack'].remove('dagger')
# add 50 to gold
inventory['gold'] +=50

print inventory