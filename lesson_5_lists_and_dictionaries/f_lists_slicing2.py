"""
You can slice a string exactly like a list! 
"""
animals = "catdogfrog"

# The first three characters of animals
cat = animals[:3]
print cat

# The fourth through sixth characters
dog = animals[3:6]
print dog

# From the seventh character to the end
frog = animals[6:10]
print frog