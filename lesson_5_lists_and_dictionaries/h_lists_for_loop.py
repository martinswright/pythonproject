"""
Write a for-loop that iterates over start_list and .append()s each number squared (x ** 2) to square_list.
Then sort square_list!
"""
start_list = [1,9,3,8,5,7]
square_list = []

for number in start_list: 
  square_list.append(number**2)
 
print square_list
  
square_list.sort()

print square_list
