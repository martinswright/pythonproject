#!/usr/bin/python
import sys
"""
A program to de-duplicate entries in our environment data. 
Read a number of files and remove any duplicate entries,
print the de-duplicated results.
"""
# 
# process the specified file and add the results to results.
#
def process(filename):
    # open the file for reading.
    infile  = open(filename, "r")
    for line in infile:
        results[line] = ""
    
    infile.close()   
 
#    
# main program start
# 
# results of the de-duplication process will end up in here
results  = {};
filenames  =  sys.argv

# remove the first array element - this is the name of the program
del filenames[0]
# for each filename process the content of the file.
for filename in filenames:
    process(filename)

# show the results 
for line in results:
    print line