class BankAccount(object):
  balance = 0
  def __init__(self, name):
    self.name = name
    
  def __repr__(self):
     return "%s's account. Balance: $%.2f" % (self.name, self.balance)
    
  def show_balance(self):
    print "%s's account. Balance: $%.2f" % (self.name, self.balance)
    
  def deposit(self, amount):
    if amount <= 0:
      print "you cannot deposit less than zero"
    else:
      print "%s's account. depositing: $%.2f" % (self.name, amount)
      self.balance += amount
      self.show_balance()
      
  def withdraw(self,amount):
    if(amount > self.balance):
      print "you don't have that much money in the account"
    else:
      self.balance -= amount
      self.show_balance()
      
my_account = BankAccount("msw")
my_account.show_balance()
my_account.deposit(100)
my_account.withdraw(50)
  