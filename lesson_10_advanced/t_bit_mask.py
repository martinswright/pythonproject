"""
A bit mask is just a variable that aids you with bitwise operations. 
A bit mask can help you turn specific bits on, turn others off,
 or just collect data from an integer about which bits are on or off.

"""
num  = 0b1100
mask = 0b0100
desired = num & mask
if desired > 0:
  print "Bit was on"
  
"""
You can also use masks to turn a bit in a number on using |
"""
a = 0b110 # 6
mask = 0b1 # 1
desired =  a | mask # 0b111, or 7

print desired
print bin(desired)