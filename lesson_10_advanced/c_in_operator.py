"""
The 'in' Operator
For iterating over lists, tuples, dictionaries, and strings, Python also includes a special keyword: in.
"""

my_dict = {
  'name': 'Nick',
  'age':  31,
  'occupation': 'Dentist',
}

for key in my_dict:
  print key + " " + str(my_dict[key])