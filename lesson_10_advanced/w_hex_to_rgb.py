"""
In this project, we'll use Bitwise operators to build a calculator that can convert
 RGB values to Hexadecimal (hex) values, and vice-versa.
"""

def rgb_hex():
  invalid_msg = "invalid input - value should be 0 to 255"
  red = int(raw_input("Enter red (R) value: "))
  if (red < 0 or red > 255):
    print invalid_msg
    return
  green = int(raw_input("Enter green (G) value: "))
  if (green < 0 or green > 255):
    print invalid_msg
    return
  blue = int(raw_input("Enter blue (B) value: "))
  if (blue < 0 or blue > 255):
    print invalid_msg
    return
  
  value = (red << 16) + (green << 8) + blue
  
  print str((hex(value))[2::]).upper()

def hex_rgb():
  hex_val = raw_input("Enter the color (six hexadecimal digits): ")
  if len(hex_val) != 6:
     print "Invalid hexidecimal value. Try again."
     return
  else:
    hex_val = int(hex_val, 16)
    two_hex_digits = 2**8
    blue = hex_val % two_hex_digits
    hex_val = hex_val >> 8
    green = hex_val % two_hex_digits
    hex_val = hex_val >> 8
    red = hex_val % two_hex_digits
    print "Red: %s Green: %s Blue: %s" % (red, green, blue)
 
def convert():
  while True:
    option = raw_input("Enter 1 to convert RGB to HEX. Enter 2 to convert HEX to RGB. Enter X to Exit: ")
    if option == '1':
      print "RGB to Hex..."
      rgb_hex()
    elif option == '2':
      print "Hex to RGB..."
      hex_rgb()
    elif option == 'X' or option == 'x':
      break

convert()

"""
example output:
Enter 1 to convert RGB to HEX. Enter 2 to convert HEX to RGB. Enter X to Exit: 1
RGB to Hex...
Enter red (R) value: 255
Enter green (G) value: 200
Enter blue (B) value: 100
FFC864

Enter 1 to convert RGB to HEX. Enter 2 to convert HEX to RGB. Enter X to Exit: 2
Hex to RGB...
Enter the color (six hexadecimal digits): aa33ff
Red: 170 Green: 51 Blue: 255

"""

