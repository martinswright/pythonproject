"""
One of the more powerful aspects of Python is that it allows for a style of programming called functional programming, 
which means that you're allowed to pass functions around just as if they were variables or values
"""
my_list = range(16)
print filter(lambda x: x % 3 == 0, my_list)

languages = ["HTML", "JavaScript", "Python", "Ruby"]

# Add arguments to the filter()
print filter(lambda x: x == "Python", languages)