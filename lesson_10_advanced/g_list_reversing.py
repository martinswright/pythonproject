"""
A negative stride progresses through the list from right to left.
"""
my_list = ['A', 'B', 'C', 'D', 'E']

# Add your code below!
backwards = my_list[::-1]

print backwards