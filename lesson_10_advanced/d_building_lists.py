"""
generate a list according to some logic—for example, a list of all the even numbers from 0 to 50
"""
evens_to_50 = [i for i in range(51) if i % 2 == 0]
print evens_to_50