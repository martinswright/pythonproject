"""
The XOR (^) or exclusive or operator compares two numbers on a bit level and returns a number where the bits of that number are turned on if either of the corresponding bits of the two numbers are 1,
 but not both.

    a:  00101010   42
    b:  00001111   15       
================
a ^ b:  00100101   37

"""

# will print 0b1011 
print bin(0b1110 ^ 0b101)
