"""
Use list slicing to print out every odd element of my_list from start to finish.
"""
my_list = range(1, 11) # List of numbers 1 - 10

# Add your code below!
print my_list[::2]