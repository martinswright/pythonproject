"""
The bitwise AND (&) operator compares two numbers on a bit level and returns a number where the bits of that number are turned on if the corresponding bits of both numbers are 1.
 For example:
    a:   00101010   42
    b:   00001111   15       
===================
 a & b:  00001010   10
"""

# will print 4
print 0b1110 & 0b101