"""
The comprehension should consist of the cubes of the numbers 1 through 10 only if the cube is evenly divisible by four.

"""
cubes_by_four = [x ** 3 for x in range(1, 11) if ((x ** 3) % 4) == 0]
print cubes_by_four