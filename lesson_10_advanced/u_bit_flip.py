"""
Using the XOR (^) operator is very useful for flipping bits. 
Using ^ on a bit with the number one will return a result where that bit is flipped.
"""
# flip all of the bits in a

a = 0b110 # 6
mask = 0b111 # 7
desired =  a ^ mask # 0b1


print desired