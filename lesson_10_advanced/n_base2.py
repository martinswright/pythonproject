"""
The binary number '1010' is 10 in base 2 because the 8's bit and the 2's bit are "on":
8's bit  4's bit  2's bit  1's bit
    1       0       1      0 
    8   +   0    +  2   +  0  = 10 

"""
print 0b1,    #1
print 0b10,   #2
print 0b11,   #3
print 0b100,  #4
print 0b101,  #5
print 0b110,  #6
print 0b111   #7
print "******"
print 0b1 + 0b11
print 0b11 * 0b11

"""
The bin function: 
"""
print "bin 1 = " + str(bin(1))
print "bin 128 = " + str(bin(128))
print "bin 255 = " + str(bin(255))

"""
the int function with base parameter
"""
# Print out the decimal equivalent of the binary 11001001.
print int("0b11001001",2)