"""
The bitwise NOT operator (~) just flips all of the bits in a single number.

"""
print bin (~0b1)

print ~1
print ~2
print ~3
print ~42
print ~123