
"""
These operators work by shifting the bits of a number over by a designated number of slots.

"""
shift_right = 0b1100
shift_left = 0b1
print bin(shift_right)
print bin(shift_left)

# shift two places right 
shift_right = shift_right >> 2

# shift two places left.

shift_left = shift_left << 2
print bin(shift_right)
print bin(shift_left)
