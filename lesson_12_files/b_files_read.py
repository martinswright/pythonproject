"""
reading data from a file.
"""
"""
Declare a variable, my_file, and set it equal to the file object returned by calling open() with both "output.txt" and "r"
"""
my_file = open("output.txt", "r")

"""
Next, print the result of using .read() on my_file
"""
print my_file.read()
"""
always close the file
"""
my_file.close()