"""
what if you want to read information from a file on your computer, and/or write that information to another file?

This process is called file I/O (the "I/O" stands for "input/output"), 
and Python has a number of built-in functions that handle this 

"""

my_list = [i ** 2 for i in range(1, 11)]
# Generates a list of squares of the numbers 1 - 10
"""
This tells Python to open output.txt in "w" mode ("w" stands for "write"). 
We stored the result of this operation in a file object,
Doing this opens the file in write-mode and prepares Python to send data into the file.
"""
f = open("output.txt", "w")

"""
The write method takes a string and writes this to the file.
"""
for item in my_list:
  f.write(str(item) + "\n")
  
"""  
You must close the file. You do this simply by calling my_file.close()
"""
f.close()
