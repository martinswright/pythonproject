"""
Using the with ... as syntax will make python close the file without us having to remember

"""
with open("text.txt", "w") as textfile:
  textfile.write("Success!")
