"""

There are two ways of iterating over a list.
for item in list:
  print item

- but you can't modify the element
 
  and 
  
  for i in range(len(list)):
  print list[i]

- you can modify the element.
  
"""

n = [3, 5, 7]

# add all the number in the array and return the total
def total(numbers):
    result = 0
    for i in range(0,len(numbers)):
        result += numbers[i]
    return result

print total(n)
# 15
print total([2,4,20,2,4,29])
# 61
