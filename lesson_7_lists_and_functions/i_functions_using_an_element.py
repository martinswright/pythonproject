"""
Using an element from a list in a function
Passing a list to a function will store it in the argument - just like with a string or a number.
"""


def list_function(x):
    x[1] = x[1] + 3
    return x


n = [3, 5, 7]
print list_function(n)
