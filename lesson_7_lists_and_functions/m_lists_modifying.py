"""
modify each element in a list. It is useful to do so in a function as 
you can easily put in a list of any length and get the same functionality.
"""

n = [3, 5, 7]


def double_list(x):
    for i in range(0, len(x)):
        x[i] = x[i] * 2
    return x
# Don't forget to return your new list!


print double_list(n)
# [6, 10, 14]