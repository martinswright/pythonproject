"""

Using multiple lists in a function is no different from just using multiple arguments in a function
  
"""
m = [1, 2, 3]
n = [4, 5, 6]


def join_lists(x, y):
    return x + y


print join_lists(m, n)
# [1, 2, 3, 4, 5, 6]