"""
You can also append or delete items of a list inside a function just
 as if you were manipulating the list outside a function.
 """
n = [3, 5, 7]


# Add your function here
def list_extender(lst):
    lst.append(9)
    return lst


print list_extender(n)
# [3, 5, 7, 9]
