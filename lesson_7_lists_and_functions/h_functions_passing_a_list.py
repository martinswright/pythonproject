"""
You pass a list to a function the same way you pass any other argument to a function.
"""
def list_function(x):
    return x


n = [3, 5, 7]
print list_function(n)
