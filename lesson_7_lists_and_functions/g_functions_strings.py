"""
An example of concatenating a String in a function.
"""
n = "Hello"


# Your function here!
def string_function(s):
    return s + " world"


print string_function(n)
