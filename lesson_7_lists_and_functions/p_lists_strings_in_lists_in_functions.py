"""

Using strings in lists in functions
  
"""
n = ["sponge", "bob"]


def join_strings(words):
    result = ""
    for w in range(len(words)):
        result += words[w]
    return result


print join_strings(n)
# spongebob
