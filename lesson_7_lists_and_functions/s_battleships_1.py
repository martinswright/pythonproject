from random import randint 

"""
build a simplified, one-player version of the classic board game Battleship! In this version of the game, there will be a 
single ship hidden in a random location on a 5x5 grid. The player will have 10 guesses to try to sink the ship.
"""

# Create a variable board and set it equal to an empty list.
board = []

# use a built-in Python function to generate our board, which we'll make into a 5 x 5 grid of all "O"s, for "ocean."
for x in range(0, 5):
  board.append(["O"] * 5)
    
# a function to print the board
def print_board(board):
  for row in board:
    print " ".join(row)

# start by displaying the board
print_board(board)

# get a random row for the battleship
def random_row(board):
  return randint(0, len(board) - 1)

# get a random column for the battleship
def random_col(board):
  return randint(0, len(board[0]) - 1)

# set the battleships location
ship_row = random_row(board)
ship_col = random_col(board)

# used for testing / showing the position.
print " hidden at row : " + str(ship_row)
print " hidden at column : " + str(ship_col)

# main loop for running the game 
for turn in range(4):

  print "Turn", turn + 1
  guess_row = int(raw_input("Guess Row: "))
  guess_col = int(raw_input("Guess Col: "))

  if guess_row == ship_row and guess_col == ship_col:
    print "Congratulations! You sank my battleship!"
    break
  else:
    if guess_row not in range(5) or guess_col not in range(5):
      print "Oops, that's not even in the ocean."
    elif board[guess_row][guess_col] == "X":
      print( "You guessed that one already." )
    else:
      print "You missed my battleship!"
      board[guess_row][guess_col] = "X"
    if (turn == 3):
      print "Game Over"
    print_board(board)
