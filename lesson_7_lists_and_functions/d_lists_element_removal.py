"""
remove elements from a list
"""
n = [1, 3, 5, 7, 9]

# pop uses the index value
n.pop(0)
print n
# [3, 5, 7, 9]

# Removed the element by its name NOT the index
n.remove(9);
print n
# [3, 5, 7]

# del removes the item at the given index but does not remove it.
del(n[1])
# Doesn't return anything
print n
# [3, 7]

