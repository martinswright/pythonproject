"""
Example of a function with more than one argument
"""

m = 5
n = 13


# Add add_function here!
def add_function(x, y):
    return x + y


print add_function(m, n)
