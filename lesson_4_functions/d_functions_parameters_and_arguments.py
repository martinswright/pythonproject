"""

A parameter is a variable that is an input to a function

The values of the parameters passed into a function are known as the arguments

"""
def power(base, exponent):  # Add your parameters here!
  result = base ** exponent
  print "%d to the power of %d is %d." % (base, exponent, result)

power(3, 4)  # Add your arguments here!