import math

# The line above is known as a generic import
# now we can use a function in the math module
print "the square root of 25 is " + str(math.sqrt(25))