def cube(number):
  """ return the cube of the given number """
  return number * number * number

def by_three(number):
  """ 
  if the number is divisible by 3 then call cube passing  
  the number and return the result, else return false 
  """
  if number % 3 == 0:
    return cube(number)
  else:
    return False
  
print " calling with 5 , result = " + str(by_three(5))

print " calling with 6 , result = " + str(by_three(6))