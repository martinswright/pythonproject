# Import *just* the sqrt function from math on line 3!
# this is known as a function import
from math import sqrt
# so now we don't need to prefix with math.
print "the square root of 25 is " + str(sqrt(25))
