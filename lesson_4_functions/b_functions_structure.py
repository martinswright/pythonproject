"""
Functions are defined with three components:
The header, which includes the def keyword, the name of the function, 
and any parameters the function requires. 

An optional comment that explains what the function does.

The body, which describes the procedures the function carries out. 
The body is indented, just like conditional statements.
 
"""

def spam():
  """ print a message """
  print "Eggs!"

spam()