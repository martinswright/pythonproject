"""
if is a conditional statement that executes some specified code after checking if its expression is True.

"""
# The indentation after on the line after the if statement tells python that this is a new block of code.
# examples
if 8 < 9:
  print "Eight is less than nine!"
  
var1 = 'Y'
var2 = 6
if var1 == 'Y' and var2 < 7:
  print "var1 is equal to 'Y' and  var2 is less than 7"
