"""
Boolean operators aren't just evaluated from left to right. Just like with arithmetic operators, there's an order of operations for boolean operators:

not is evaluated first;
and is evaluated next;
or is evaluated last.
For example, True or not False and False returns True. 

"""
# examples
# False
bool_one = False or not True and True
print "bool_one : " + str(bool_one)

# True
bool_two = False and not True or True
print "bool_two : " + str(bool_two)

# True
bool_three = True and not (False or False)
print "bool_three : " + str(bool_three)

# False
bool_four = False or not (True and True)
print "bool_four : " + str(bool_four)