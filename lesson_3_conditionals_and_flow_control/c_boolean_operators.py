"""
Boolean operators compare statements and result in boolean values. There are three boolean operators:

and, which checks if both the statements are True;
or, which checks if at least one of the statements is True;
not, which gives the opposite of the statement.


Truth tables 
      AND
  A     B       A AND B
  True  True     True
  True  False    False
  False True     False
  False False    False
  
      OR 
  A     B       A OR B
  True  True     True
  True  False    True
  False True     True
  False False    False
      
          NOT 
  A         NOT A
  True      False
  False     True
"""
