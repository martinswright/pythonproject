"""
The boolean operator not returns True for false statements and False for true statements.

For example:

not False will evaluate to True, while not 41 > 40 will return False.
"""
# examples
# False
bool_one = not True
print "bool_one : " + str(bool_one)

# True
bool_two = not False 
print "bool_two : " + str(bool_two)

# True
bool_three = not 7 < 4
print "bool_three : " + str(bool_three)

# False
bool_four = not 5 == 5
print "bool_four : " + str(bool_four)