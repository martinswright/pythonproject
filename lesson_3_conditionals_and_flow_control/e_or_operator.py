"""
The boolean operator or returns True when at least one expression on either side of or is true. For example:

1 < 2 or 2 > 3 is True;
1 > 2 or 2 > 3 is False.
"""
# examples
# False
bool_one = 5 < 3 or 7 > 9
print "bool_one : " + str(bool_one)

# True
bool_two = False or True
print "bool_two : " + str(bool_two)

# False
bool_three = False or 7 < 4
print "bool_three : " + str(bool_three)

#True
bool_four = True or True
print "bool_four : " + str(bool_four)

#True
bool_five = 5 == 5 or 3 == 3
print "bool_five : " + str(bool_five)
