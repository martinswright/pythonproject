"""

The else statement complements the if statement. 
An if/else pair says: "If this expression is true, 
run this indented code block; otherwise, run this code after the else statement."

"""
# example
if 8 > 9:
  print "I don't printed!"
else:
  print "I get printed!"
  
# other example
name = "fred"

if name == "fred":
    print "name is set to fred"
else:
    print "name is not fred"
    print "name is " + name + " instead."


name = "barney"
if name == "fred":
    print "name is set to fred"
else:
    print "name is not fred"
    print "name is " + name + " instead."
    
  