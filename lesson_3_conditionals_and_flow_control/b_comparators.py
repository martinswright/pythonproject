"""
 The simplest aspect of control flow is using comparators 
 there are 6
"""
num_one = 1
num_two = 2
# Equal ==
bool_one = num_one == num_two
print "bool_one : " + str(bool_one)

# Not Equal to !=
bool_two = num_one != num_two
print "bool_two : " + str(bool_two)

# Less than <
bool_three = num_one < num_two
print "bool_three : " + str(bool_three)

# less than or equal to <=
bool_four = num_one <= num_two
print "bool_four : " + str(bool_four)

# greater than >
bool_five = num_one > num_two
print "bool_five : " + str(bool_five)

# greater than or equal to
bool_six = num_one >= num_two
print "bool_six : " + str(bool_six)