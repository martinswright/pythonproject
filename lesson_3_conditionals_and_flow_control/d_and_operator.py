"""
The boolean operator and returns True when the expressions on both sides of and are true. For instance:

1 < 2 and 2 < 3 is True;
1 < 2 and 2 > 3 is False.
"""
# examples
# True
bool_one = 5 < 3 and 7 > 9
print "bool_one : " + str(bool_one)

# False
bool_two = False and True
print "bool_two : " + str(bool_two)

# False
bool_three = False and 7 < 4
print "bool_three : " + str(bool_three)

#True
bool_four = True and True
print "bool_four : " + str(bool_four)

#True
bool_five = 5 == 5 and 3 == 3
print "bool_five : " + str(bool_five)
