"""
elif is short for "else if." 
It means exactly what it sounds like: "otherwise, if the following expression is true, do this!"

"""
# in this example output will be "var1 is equal to 9" 
var1 = 9 

if var1 > 9:
  print "var1 is greater than 9"
elif var1 == 9:
  print "var1 is equal to 9"      
elif var1 < 9:
  print "var1 is less than 9"
else:
  print "none of the above are True"
